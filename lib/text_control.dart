import 'package:assignment_one/text_output.dart';
import 'package:flutter/material.dart';

class TextControl extends StatefulWidget {
  @override
  _TextControlState createState() => _TextControlState();
}

class _TextControlState extends State<TextControl> {
  String _mainText = 'This is firt Asignment!';
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        RaisedButton(
          child: Text('Change Text'),
          onPressed: () {
            setState(() {
              _mainText = 'This change!';
            });
          },
        ),
        Text(_mainText),
        TextOutput(_mainText)
      ],
    );
  }
}
